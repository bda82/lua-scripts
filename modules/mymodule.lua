local mymodule = {}

function mymodule.log(message)
	print(message)
end

function mymodule.sum(a, b)
	return a + b
end

return mymodule