#!/usr/bin/env tarantool

MySchema = require "MySchema"

box.cfg{listen = 3301}

MySchema.create_database()

function InsertHandler(req)
    -- Not ready yet
end

function ListHandler(req)
    local MS = MySchema.list()
    local responseData = {}
    for id, mesData in pairs(MS) do
        table.insert(responseData, { ["id"] = id, ["data"] = mesData })
    end
    return req:render{ json = responseData }
end

local server = require('http.server').new(nil, 3303) -- listen *:3303
server:route({ path = '/insert' }, InsertHandler)
server:route({ path = '/list' }, ListHandler)
server:start()