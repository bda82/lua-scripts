#!/usr/bin/env tarantool

box.cfg{listen = 3301}

local MySchema = {}

function MySchema.create_database()
    local myspace = box.schema.space.create("myspace", {
        if_not_exists = true,
        format = {
            { name = 'id', type = 'string'},
            { name = 'ts', type = 'number' },
            { name = 'data', type = 'string' },
        }
    })

    myspace:create_index("id_index", {
        type = 'tree',
        parts = {1, "string"},
        if_not_exists = true
    })
end

function MySchema.insert(id, ts, data)
    return box.space.myspace:insert({id, ts, data})
end

function MySchema.list()
    return box.space.myspace.index.id_index:select({})
end

return MySchema